import React, { Component } from 'react';
import './App.css';
import AddPanel from "../components/AddPanel/AddPanel";
import ViewPanel from "../components/ViewPanel/ViewPanel";

class App extends Component {

  state = {
    movies: [],
    currentMovie: ''
  };

  handlerChangeCurrentMovie = (event) => {
    let currentText = event.target.value;
    this.setState(prevState => {return {currentMovie: currentText}});
  };

  handlerAddNewMovie = () => {
    if (this.state.currentMovie.length > 0) {
      const moviesCopy = [...this.state.movies];
      const newMovie = {id: Date.now(), movie: this.state.currentMovie};
      moviesCopy.push(newMovie);
      this.setState(prevState => {return {movies: moviesCopy, currentMovie: ''}});
    }
  };

  handlerChangeMovieInList = (event, id) => {
    const index = this.state.movies.findIndex(p => p.id === id);
    const moviesCopy = [...this.state.movies];
    const itemCopy = {...moviesCopy[index]};
    let inputText = event.target.value;
    itemCopy.movie = inputText;
    moviesCopy[index] = itemCopy;
    this.setState(prevState => {return {movies: moviesCopy}});
  };

  handlerRemoveMovieFromList = (id) => {
    const index = this.state.movies.findIndex(p => p.id === id);
    const moviesCopy = [...this.state.movies];
    moviesCopy.splice(index, 1);
    this.setState(prevState => {return {movies: moviesCopy}});
  };

  componentWillMount() {
    const state = localStorage.getItem('state');
    if (state) {
      this.setState(JSON.parse(state))
    }
  }

  componentDidUpdate() {
    const state = JSON.stringify(this.state);
    localStorage.setItem('state', state);
  }

  render() {
    return (
      <div className='App'>
        <AddPanel
          text={this.state.currentMovie}
          clicked={this.handlerAddNewMovie}
          change={(event) => this.handlerChangeCurrentMovie(event)}/>
        <ViewPanel
          movies={this.state.movies}
          changeMovie={this.handlerChangeMovieInList}
          remove={this.handlerRemoveMovieFromList}/>
      </div>
    );
  }
}

export default App;
