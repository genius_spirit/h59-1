import React, {Component, Fragment} from 'react';
import './ViewPanel.css';
import ViewItem from "./ViewItem/ViewItem";

class ViewPanel extends Component{

  render() {
    return (
      <Fragment>
        <div className='title'>My watch list:</div>
        <div className="ViewPanel">
          { this.props.movies.map(item => {
            return (
              <div key={item.id} className='ViewPanel-item'>
                <ViewItem changeMovie={this.props.changeMovie} movieTitle={item.movie} movieId={item.id}/>
                <i className="material-icons ViewPanel-remove"
                   onClick={() => this.props.remove(item.id)}>delete</i>
              </div>
            )
          })
          }
        </div>
      </Fragment>
    )
  }
}

export default ViewPanel;